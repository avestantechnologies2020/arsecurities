<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\product;

class productController extends Controller
{
    public function index(){

        return product ::all();
    }

    public function store(Request $request){
        $product = new product;
        $product->product_category = request('product_category');
        $product->product_name = request('product_name');
        $product->save();
        return response()->json('saved..!', 201);
    }

    public function show(Request $request){
       return product::find($request->id);
    }

    public function delete(Request $request){

        $product = product::find($request->id);
        $product->delete();
        return response()->json(null, 204);
    }

    public function update(Request $request){

        product::where('id',$request->id)->update(['product_category'=>$request->product_category,'product_name'=>$request->product_name]);
        return response()->json('done', 200);
    }
}
