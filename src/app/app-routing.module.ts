import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutusComponent } from './aboutus/aboutus.component';

import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { RegisterComponent } from './register/register.component';
import { ViewProductComponent } from './view-product/view-product.component';
import { ViewcategoryComponent } from './viewcategory/viewcategory.component';

const routes: Routes = [
  {path:"",component:HomeComponent},
  {path:"about",component:AboutusComponent},
  {path:"contact",component:ContactComponent},
  
  {path:"login",component:LoginComponent},
  {path:"register",component:RegisterComponent},
  {path:":id",component:ViewProductComponent},
  {path:"cat/:id",component:ViewcategoryComponent},
  {path:"**",component:PagenotfoundComponent},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
