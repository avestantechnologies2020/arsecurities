import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
   
  search1="";
  all;
  constructor(private _router:Router,private _product:ProductService) { 
    
    this.all = this._product.data.img;
      
      
  }
  
  search(){

    this._product.products.data = [];

    for(var i=0;i<this.all.length;i++){
         
      if(this.search1==this.all[i].cat)
         {
           this._product.products.data.push(this.all[i]) 
           
         }
      
    }
     this._router.navigate(["cat", this.search1]);
     this.search1="";
  }
  navigate(d){   

    this._product.products.data = [];

    for(var i=0;i<this.all.length;i++){
         
      if(d==this.all[i].cat)
         {
           this._product.products.data.push(this.all[i]) 
           
         }
         
    }
    
    this._router.navigate(["cat",d]);
  }
  
  options: string[] = ['Artificial Intelligence Products', 'Biometric Time Attendance', 'Smart Card & RFID Reader','Fingerprint & IRIS Reader', 'RFID TAGS & CARDS', 'POS','Entrance Automation', 'GPS & Navigation System', 'CCTV Camera','Paper Rolls'];

  ngOnInit() {
  }
  
}
