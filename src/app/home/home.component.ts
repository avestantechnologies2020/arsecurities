import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
data:any;
ai;
pos;
entrance;
smart_card;
rfid_tag;
all;
new_arrival;
  constructor(private _product:ProductService,private _router:Router) {
    
    this.data = this._product.data;
    this.ai = this._product.aiproducts;
    this.pos = this._product.pos;
    this.entrance = this._product.entrance;
    this.smart_card = this._product.smart_card;
    this.rfid_tag = this._product.rfid_tag;
    this.all = this._product.all;
    this.new_arrival = this._product.new_arrival;
   }
  ngOnInit(): void {
    
  }

  drop1(){
    var list =<HTMLUListElement>document.getElementById("list");
     var v = list.style.display;
     if(v = "none"){
      list.style.display="block";
     }
     alert(v)
  }

  drop2(){
    var list =<HTMLUListElement>document.getElementById("list");
     var v = list.style.display;
     if(v = "block"){
      list.style.display="none";
     }
     alert(v)
  }
   

  viewproduct(d){
    debugger
    this._product.dataSource.next(d);
    this._router.navigate(["",d.name]);
    
  }

}
