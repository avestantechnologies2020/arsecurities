import { Component, OnInit } from '@angular/core';

import { ProductService } from '../product.service';

@Component({
  selector: 'app-view-product',
  templateUrl: './view-product.component.html',
  styleUrls: ['./view-product.component.css']
})
export class ViewProductComponent implements OnInit {

data:any;
  constructor(private _product:ProductService) { 

  this._product.dataSource.subscribe((res:any)=>{
    
    this.data = res;

  })

  }

  ngOnInit() {
  }

}
