import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-viewcategory',
  templateUrl: './viewcategory.component.html',
  styleUrls: ['./viewcategory.component.css']
})
export class ViewcategoryComponent implements OnInit {


 parameter;
 all;
 
  constructor(private _product:ProductService,private _router:Router,private _activatedRoute:ActivatedRoute) {

  }
   
  viewproduct(d){
    
    this._product.dataSource.next(d);
    this._router.navigate(["",d.name]);
    
  }

  ngOnInit() {
  }

  ngDoCheck() {
    this._activatedRoute.params.subscribe((data)=>{
      this.parameter = data.id;
    })
    this.all = this._product.products.data;
  }

}
